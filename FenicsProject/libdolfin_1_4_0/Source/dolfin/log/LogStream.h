// Copyright (C) 2003-2009 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Garth N. Wells 2005.
//
// First added:  2003-03-13
// Last changed: 2009-08-11

#ifndef __LOG_STREAM_H
#define __LOG_STREAM_H

// modified 14.01.2015 to compile VS2013 project
#include "DolfinExport.h"

#include <complex>
#include <string>
#include <sstream>

namespace dolfin
{

  class DOLFINAPI Variable;
  class DOLFINAPI MeshEntity;
  class DOLFINAPI MeshEntityIterator;
  class DOLFINAPI Point;

  /// This class provides functionality similar to standard C++
  /// streams (std::cout, std::endl) for output but working through
  /// the DOLFIN log system.

  class DOLFINAPI LogStream
  {
  public:

    /// Stream types
    enum Type {COUT, ENDL};

    /// Create log stream of given type
    LogStream(Type type);

    /// Destructor
    ~LogStream();

    /// Output for log stream
    LogStream& operator<< (const LogStream& stream);

    /// Output for string
    LogStream& operator<< (const std::string& s);

    /// Output for int
    LogStream& operator<< (int a);

    /// Output for unsigned int
    LogStream& operator<< (unsigned int a);

    /// Output for long int
    LogStream& operator<< (long int a);

    /// Output for long int
    LogStream& operator<< (long unsigned int a);

    /// Output for double
    LogStream& operator<< (double a);

    /// Output for std::complex<double>
    LogStream& operator<< (std::complex<double> z);

    /// Output for variable (calling str() method)
    LogStream& operator<< (const Variable& variable);

    /// Output for mesh entity (not subclass of Variable for efficiency)
    LogStream& operator<< (const MeshEntity& entity);

    /// Output for point (not subclass of Variable for efficiency)
    LogStream& operator<< (const Point& point);

    void setprecision(std::streamsize n);

	// modified 15.01.2015 to compile VS2013 project
#if defined  _M_X64
	LogStream& operator<< (const size_t a);
#endif

  private:

    // Type of stream
    Type _type;

    // Buffer
    std::stringstream buffer;

  };

  /// dolfin::cout
  //extern LogStream cout;
  __declspec(dllimport) extern LogStream cout;

  /// dolfin::endl;
  //extern LogStream endl;
  __declspec(dllimport) extern LogStream endl;

}

#endif
