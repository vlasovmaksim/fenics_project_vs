// Copyright (C) 2009-2011 Johan Hake
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Anders Logg, 2011
// Modified by Garth N. Wells, 2013
//
// First added:  2009-09-03
// Last changed: 2013-11-01

#ifndef __DOLFIN_DEFINES_H
#define __DOLFIN_DEFINES_H

// modified 14.01.2015 to compile VS2013 project
#include "DolfinExport.h"

#include<string>

namespace dolfin
{

  /// Return DOLFIN version string
  DOLFINAPI std::string dolfin_version();

  /// Return git changeset hash (returns "unknown" if changeset is
  /// not known)
  DOLFINAPI std::string git_commit_hash();

  /// Return true if DOLFIN is compiled with OpenMP
  DOLFINAPI bool has_openmp();

  /// Return true if DOLFIN is compiled with MPI
  DOLFINAPI bool has_mpi();

  /// Return true if DOLFIN is compiled with PETSc
  DOLFINAPI bool has_petsc();

  /// Return true if PETSc SNES is enabled
  DOLFINAPI bool has_petsc_snes();
  
  /// Return true if PETSc TAO is enabled
  DOLFINAPI bool has_petsc_tao();
  
  /// Return true if DOLFIN is compiled with SLEPc
  DOLFINAPI bool has_slepc();

  /// Return true if DOLFIN is compiled with Trilinos
  DOLFINAPI bool has_trilinos();

  /// Return true if DOLFIN is compiled with Scotch
  DOLFINAPI bool has_scotch();

  /// Return true if DOLFIN is compiled with CGAL
  DOLFINAPI bool has_cgal();

  /// Return true if DOLFIN is compiled with Umfpack
  DOLFINAPI bool has_umfpack();

  /// Return true if DOLFIN is compiled with Cholmod
  DOLFINAPI bool has_cholmod();

  /// Return true if DOLFIN is compiled with ParMETIS
  DOLFINAPI bool has_parmetis();

  /// Return true if DOLFIN is compiled with ZLIB
  DOLFINAPI bool has_zlib();

  /// Return true if DOLFIN is compiled with HDF5
  DOLFINAPI bool has_hdf5();

  /// Return true if DOLFIN is compiled with Exodus
  DOLFINAPI bool has_exodus();

}

#endif
