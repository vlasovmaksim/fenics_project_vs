// Auto generated SWIG file for Python interface of DOLFIN
//
// Copyright (C) 2012 Johan Hake
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
//


// The PyDOLFIN extension module for the function module
%module(package="dolfin.cpp.function", directors="1") function

// Define module name for conditional includes
#define FUNCTIONMODULE

%{

// Include types from dependent modules

// #include types from common submodule of module common
#include "dolfin/common/Array.h"
#include "dolfin/common/Variable.h"
#include "dolfin/common/Hierarchical.h"
#include "dolfin/common/MPI.h"

// #include types from mesh submodule of module mesh
#include "dolfin/mesh/MeshData.h"
#include "dolfin/mesh/Mesh.h"
#include "dolfin/mesh/MeshEntity.h"
#include "dolfin/mesh/Cell.h"
#include "dolfin/mesh/LocalMeshData.h"
#include "dolfin/mesh/BoundaryMesh.h"

// #include types from geometry submodule of module mesh
#include "dolfin/geometry/Point.h"

// #include types from la submodule of module la
#include "dolfin/la/LinearAlgebraObject.h"
#include "dolfin/la/GenericTensor.h"
#include "dolfin/la/GenericVector.h"
#include "dolfin/la/Vector.h"

// #include types from fem submodule of module fem
#include "dolfin/fem/GenericDofMap.h"
#include "dolfin/fem/DofMap.h"
#include "dolfin/fem/FiniteElement.h"
#include "dolfin/fem/CCFEMDofMap.h"

// Include types from present module function

// #include types from function submodule
#include "dolfin/function/GenericFunction.h"
#include "dolfin/function/Expression.h"
#include "dolfin/function/FunctionAXPY.h"
#include "dolfin/function/Function.h"
#include "dolfin/function/FunctionSpace.h"
#include "dolfin/function/SubSpace.h"
#include "dolfin/function/Constant.h"
#include "dolfin/function/SpecialFunctions.h"
#include "dolfin/function/SpecialFacetFunction.h"
#include "dolfin/function/CCFEMFunctionSpace.h"
#include "dolfin/function/FunctionAssigner.h"
#include "dolfin/function/assign.h"
#include "dolfin/function/CCFEMFunction.h"
#include "dolfin/function/LagrangeInterpolator.h"

// #include types from math submodule
#include "dolfin/math/basic.h"
#include "dolfin/math/Lagrange.h"
#include "dolfin/math/Legendre.h"

// #include types from ale submodule
#include "dolfin/ale/ALE.h"
#include "dolfin/ale/MeshDisplacement.h"

// #include types from graph submodule
#include "dolfin/graph/Graph.h"
#include "dolfin/graph/GraphBuilder.h"
#include "dolfin/graph/BoostGraphOrdering.h"
#include "dolfin/graph/SCOTCH.h"

// NumPy includes
#define PY_ARRAY_UNIQUE_SYMBOL PyDOLFIN_FUNCTION
#include <numpy/arrayobject.h>
%}

%init%{
import_array();
%}

// Include global SWIG interface files:
// Typemaps, shared_ptr declarations, exceptions, version
%include "dolfin/swig/globalincludes.i"

// %import types from submodule common of SWIG module common
%include "dolfin/swig/common/pre.i"
%import(module="common") "dolfin/common/Array.h"
%import(module="common") "dolfin/common/Variable.h"
%import(module="common") "dolfin/common/Hierarchical.h"
%import(module="common") "dolfin/common/MPI.h"

// %import types from submodule mesh of SWIG module mesh
%include "dolfin/swig/mesh/pre.i"
%import(module="mesh") "dolfin/mesh/MeshData.h"
%import(module="mesh") "dolfin/mesh/Mesh.h"
%import(module="mesh") "dolfin/mesh/MeshEntity.h"
%import(module="mesh") "dolfin/mesh/Cell.h"
%import(module="mesh") "dolfin/mesh/LocalMeshData.h"
%import(module="mesh") "dolfin/mesh/BoundaryMesh.h"

// %import types from submodule geometry of SWIG module mesh
%include "dolfin/swig/geometry/pre.i"
%import(module="mesh") "dolfin/geometry/Point.h"

// %import types from submodule la of SWIG module la
%include "dolfin/swig/la/pre.i"
%import(module="la") "dolfin/la/LinearAlgebraObject.h"
%import(module="la") "dolfin/la/GenericTensor.h"
%import(module="la") "dolfin/la/GenericVector.h"
%import(module="la") "dolfin/la/Vector.h"

// %import types from submodule fem of SWIG module fem
%include "dolfin/swig/fem/pre.i"
%import(module="fem") "dolfin/fem/GenericDofMap.h"
%import(module="fem") "dolfin/fem/DofMap.h"
%import(module="fem") "dolfin/fem/FiniteElement.h"
%import(module="fem") "dolfin/fem/CCFEMDofMap.h"

// Turn on SWIG generated signature documentation and include doxygen
// generated docstrings
//%feature("autodoc", "1");
%include "dolfin/swig/function/docstrings.i"
%include "dolfin/swig/math/docstrings.i"
%include "dolfin/swig/ale/docstrings.i"
%include "dolfin/swig/graph/docstrings.i"

// %include types from submodule function
%include "dolfin/swig/function/pre.i"
%include "dolfin/function/GenericFunction.h"
%include "dolfin/function/Expression.h"
%include "dolfin/function/FunctionAXPY.h"
%include "dolfin/function/Function.h"
%include "dolfin/function/FunctionSpace.h"
%include "dolfin/function/SubSpace.h"
%include "dolfin/function/Constant.h"
%include "dolfin/function/SpecialFunctions.h"
%include "dolfin/function/SpecialFacetFunction.h"
%include "dolfin/function/CCFEMFunctionSpace.h"
%include "dolfin/function/FunctionAssigner.h"
%include "dolfin/function/assign.h"
%include "dolfin/function/CCFEMFunction.h"
%include "dolfin/function/LagrangeInterpolator.h"
%include "dolfin/swig/function/post.i"

// %include types from submodule math
%include "dolfin/math/basic.h"
%include "dolfin/math/Lagrange.h"
%include "dolfin/math/Legendre.h"

// %include types from submodule ale
%include "dolfin/swig/ale/pre.i"
%include "dolfin/ale/ALE.h"
%include "dolfin/ale/MeshDisplacement.h"

// %include types from submodule graph
%include "dolfin/graph/Graph.h"
%include "dolfin/graph/GraphBuilder.h"
%include "dolfin/graph/BoostGraphOrdering.h"
%include "dolfin/graph/SCOTCH.h"
%include "dolfin/swig/graph/post.i"

