#ifndef __DOLFIN_EXPORT_H
#define __DOLFIN_EXPORT_H

#ifdef USEDOLFINLIBRARY
#ifdef  DOLFINLIBRARY_EXPORTS 
#define DOLFINAPI __declspec(dllexport)
#else
#define DOLFINAPI __declspec(dllimport)
#endif
#else
#define DOLFINAPI
#endif


#endif