#include <dolfin.h>
#include "Poisson.h"


using namespace dolfin;

int main()
{
  UnitSquareMesh mesh(32, 32);
  //dolfin::UnitCubeMesh mesh(2, 2, 2);

  Poisson::FunctionSpace V(mesh);

  dolfin::Function u(V);
  dolfin::Function u_0(V);

  dolfin::LagrangeInterpolator lagr;
  lagr.interpolate(u, u_0);





  
  //std::size_t geom_dim = 3;

  //std::size_t z = geom_dim - 1;

  //std::size_t v_size = v.size();

  //std::size_t num_elem = (v_size / geom_dim);

  //for (std::size_t i = z; i < v_size; i += geom_dim) {
  //  cout << "v[i] = " << v[i] << "; i = " << i << '\n';
  //}




  //for (std::size_t i = geom_dim - 1; i < num; i += geom_dim) {
  //  sub_mesh_coordinates[i] = 0.5;
  //}

  

  //std::size_t i = 2;

 /* for (std::size_t j = gdim0 - 1; j < coordinates_size; j += gdim0) {

    x_min_max[i] = std::min(x_min_max[i], coordinates[j]);
    x_min_max[gdim0 + i] = std::max(x_min_max[gdim0 + i], coordinates[j]);
  }*/

  
#pragma region It works.
  // Create bounding box of mesh0

  //const Mesh& mesh0 = mesh;
  //const std::size_t gdim0 = mesh0.geometry().dim();
  //  
  //std::vector<double> x_min_max(2 * gdim0);
  //std::vector<double> coordinates = mesh0.coordinates();

  //std::size_t coordinates_size = coordinates.size();

  //for (std::size_t i = 0; i < gdim0; i++) {
  //  for (std::size_t j = i; j < coordinates_size; j += gdim0) {

  //    x_min_max[i] = std::min(x_min_max[i], coordinates[j]);
  //    x_min_max[gdim0 + i] = std::max(x_min_max[gdim0 + i], coordinates[j]);
  //  }
  //}
#pragma endregion



#pragma region Backup
  //std::vector<double> x_min_max(2 * gdim0);
  //std::vector<double> coordinates = mesh0.coordinates();
  //for (std::size_t i = 0; i<gdim0; i++) {
  //  for (std::vector<double>::const_iterator it = coordinates.begin() + i;
  //    it < coordinates.end(); it += gdim0) {
  //    x_min_max[i] = std::min(x_min_max[i], *it);
  //    x_min_max[gdim0 + i] = std::max(x_min_max[gdim0 + i], *it);
  //  }
  //}  
#pragma endregion



  std::cin.get();

  return 0;
}
