// Copyright (C) 2006-2011 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2006-02-07
// Last changed: 2013-03-11
//
// This demo program solves Poisson's equation
//
//     - div grad u(x, y) = f(x, y)
//
// on the unit square with source f given by
//
//     f(x, y) = 10*exp(-((x - 0.5)^2 + (y - 0.5)^2) / 0.02)
//
// and boundary conditions given by
//
//     u(x, y) = 0        for x = 0 or x = 1
// du/dn(x, y) = sin(5*x) for y = 0 or y = 1

#include <dolfin.h>
#include "Poisson.h"

using namespace dolfin;

// Source term (right-hand side)
class Source : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const
  {
    double dx = x[0] - 0.5;
    double dy = x[1] - 0.5;
    values[0] = 10*exp(-(dx*dx + dy*dy) / 0.02);
  }
};

// Normal derivative (Neumann boundary condition)
class dUdN : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = sin(5*x[0]);
  }
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundary : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return x[0] < DOLFIN_EPS || x[0] > 1.0 - DOLFIN_EPS;
  }
};

int main()
{
  //set_log_level(10);

  parameters["linear_algebra_backend"] = "PETSc";
  parameters["mesh_partitioner"] = "ParMETIS";

  parameters["num_threads"] = 0;
  //parameters["num_threads"] = 2;
  //parameters["num_threads"] = 6;

  // Create mesh and function space
  //UnitSquareMesh mesh(100, 100);
  //UnitSquareMesh mesh(32, 32);
  //UnitSquareMesh mesh(2, 2);
  UnitSquareMesh mesh(8, 8);
  Poisson::FunctionSpace V(mesh);

  // Define boundary condition
  Constant u0(0.0);
  DirichletBoundary boundary;
  DirichletBC bc(V, u0, boundary);

  // Define variational forms
  Poisson::BilinearForm a(V, V);
  Poisson::LinearForm L(V);
    
  Source f;
  dUdN g;
  L.f = f;
  L.g = g;

  // Solver crashes if then solve a problem.
  //Matrix A;
  //Vector b;

  //assemble(A, a);
  //assemble(b, L);
    //assemble_system(A, b, a, L);

  // Compute solution
  Function u(V);
  Function u_1(V);

  std::shared_ptr<Function> u_pytr;
  std::shared_ptr<Function> u_1_pytr;

  assign(u_pytr, u_1_pytr);

  //solve(a == L, u, bc);

  LinearVariationalProblem problem(a, L, u, bc);
  LinearVariationalSolver solver(problem);
  solver.parameters["linear_solver"] = "gmres";

  // Doesn't work in parallel!
  //solver.parameters["preconditioner"] = "ilu";

  solver.solve();

  //bc.apply(A, b);

  // Solver crashes.
  //solve(A, *u.vector(), b, "gmres", "ilu");

  // Save solution in VTK format
  File file("poisson.pvd");
  file << u;

  // Plot solution
  plot(u);
  interactive();

  return 0;
}
