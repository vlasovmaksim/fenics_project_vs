// Copyright (C) 2006-2011 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2006-02-07
// Last changed: 2013-03-11
//
// This demo program solves Poisson's equation
//
//     - div grad u(x, y) = f(x, y)
//
// on the unit square with source f given by
//
//     f(x, y) = 10*exp(-((x - 0.5)^2 + (y - 0.5)^2) / 0.02)
//
// and boundary conditions given by
//
//     u(x, y) = 0        for x = 0 or x = 1
// du/dn(x, y) = sin(5*x) for y = 0 or y = 1

#include <dolfin.h>
#include "Poisson.h"

#include <iostream>
//#include <string>

using namespace dolfin;

// Source term (right-hand side)
class Source : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const
  {
    double dx = x[0] - 0.5;
    double dy = x[1] - 0.5;
    values[0] = 10*exp(-(dx*dx + dy*dy) / 0.02);
  }
};

// Normal derivative (Neumann boundary condition)
class dUdN : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const
  {
    values[0] = sin(5*x[0]);
  }
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundary : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return x[0] < DOLFIN_EPS || x[0] > 1.0 - DOLFIN_EPS;
  }
};

void Pause()
{
	std::cout << std::endl << "Press any key to continue...";
	std::cin.get();
	//    getchar();
}

int main()
{
	//  set_log_level(ERROR);
	//	set_log_level(WARNING);
	//set_log_level(INFO);
	//  set_log_level(PROGRESS);
		set_log_level(DBG);

	// Test for MPI, PETSC, PARMETIS (HAS_MPI; HAS_PETSC; HAS_PARMETIS)

	parameters["linear_algebra_backend"] = "PETSc";
	//parameters["mesh_partitioner"] = "ParMETIS";
	//parameters["linear_algebra_backend"] = "uBLAS";

	//GlobalParameters temp = dolfin::parameters;
  GlobalParameters temp = dolfin::GlobalParameters();

  // Create mesh and function space
  //UnitSquareMesh mesh(32, 32);
  //UnitSquareMesh mesh(100, 100);
  //UnitSquareMesh mesh(1000, 1000);

  // Test for HDF5 (HAS_HDF5)
  Mesh mesh;
  std::string MESH_FILENAME = "poisson_mesh.xdmf";
  File* MeshFile = new File(MESH_FILENAME);
    
  // Write mesh to file
  //*MeshFile << mesh;

  // Read mesh from file
  *MeshFile >> mesh;

  // Test for VTK (HAS_VTK)
  File meshfilepvd("mesh.pvd");
  meshfilepvd << mesh;

  // Test for VTK (HAS_VTK)
  plot(mesh);
  interactive(true);

  Poisson::FunctionSpace V(mesh);

  // Define boundary condition
  Constant u0(0.0);
  DirichletBoundary boundary;
  DirichletBC bc(V, u0, boundary);

  // Define variational forms
  Poisson::BilinearForm a(V, V);
  Poisson::LinearForm L(V);

  Source f;
  dUdN g;
  L.f = f;
  L.g = g;

  // Compute solution
  Function u(V);



  // It works
  Matrix A;
  assemble(A, a);

  Vector b;
  assemble(b, L);

  std::vector<DirichletBC*> bcs;
  bcs.push_back(&bc);

  for (std::size_t i = 0; i < bcs.size(); i++)
	  bcs[i]->apply(A, b);

  Timer TimerSolve = Timer("TimerSolve");
  TimerSolve.start();

  Parameters parameters_my("empty_parameters");

  //solve(A, *u.vector(), b, "gmres", "default");
  solve(a == L, u, bc);
  //solve(a == L, u, bc, parameters_my);
  //solve(a == L, u, bc, parameters);

  TimerSolve.stop();
  double TimerTemp = TimerSolve.value();
  std::cout << "TimerSolve = " << TimerTemp;

  // Save solution in VTK format
  //File file("poisson.pvd");

  // Test for ZLIB (HAS_ZLIB)
  File file("poisson.pvd", "compressed");
  file << u;

  //list_timings();

  // Plot solution
  //plot(u);
  //interactive();

  Pause();

  return 0;
}
