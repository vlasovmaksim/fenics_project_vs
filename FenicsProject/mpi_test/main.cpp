#include <dolfin.h>


int main() {

  size_t rank;
  size_t size;
  size_t len;
  size_t temp;


  MPI_Init(NULL, NULL);

  rank = dolfin::MPI::rank(MPI_COMM_WORLD);
  size = dolfin::MPI::size(MPI_COMM_WORLD);

  printf("rank = %d; size = %d \n", rank, size);

// For debug purpose. 
// Before input any number you could attach to the debug process to debug it.
// Works only in the Debug mode.
#ifdef _DEBUG
  if (size > 1 && rank == 0) {
    std::cout << "Please input number: ";
    std::cin >> temp;
  }

  dolfin::MPI::barrier(MPI_COMM_WORLD);
#endif


#pragma region dolfin::MPI::sum
  double sum = 1.0;

  std::cout << "rank = " << rank << "; sum = " << sum << std::endl;

  sum = dolfin::MPI::sum<double>(MPI_COMM_WORLD, sum);

  std::cout << "rank = " << rank << "; sum after dolfin::MPI::sum = " << sum << std::endl;
#pragma endregion


#pragma region dolfin::MPI::max
  double eps = 0;

  if (size > 1 && rank == 1) {
    eps = 1;
  }

  std::cout << "rank = " << rank << "; eps = " << eps << std::endl;

  eps = dolfin::MPI::max(MPI_COMM_WORLD, eps);

  std::cout << "rank = " << rank << "; eps after dolfin::MPI::max = " << eps << std::endl;
#pragma endregion


  MPI_Finalize();
}
