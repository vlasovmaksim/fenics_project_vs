#ifndef __DOLFIN_EXPORT_H
#define __DOLFIN_EXPORT_H

#ifdef USEDLL
	#ifdef  LIBDOLFIN_EXPORTS 
	#define DLLEXPORT __declspec(dllexport)
	#else
	#define DLLEXPORT __declspec(dllimport)
	#endif
#else
	#define DLLEXPORT
#endif

#endif