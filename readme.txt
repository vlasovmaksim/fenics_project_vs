The goal of this project is to compile Fenics Project dolfin dynamic library (dll) under the Visual Studio (current version 2013).

You can download free version of the Visual Studio from here:
http://www.visualstudio.com/products/visual-studio-community-vs

_____________________________________________________________________________________
# Updated 23.05.2016 #

Added "boost_1_56_0_lib-msvc-12.0.rar" to the downloads.
_____________________________________________________________________________________
# Updated 02.04.2015 #

I created a new "release 1.2" of dolfin 1.4.0 library for windows
_____________________________________________________________________________________
# Updated 19.01.2015 #

I created a new "release 1.1" of dolfin 1.4.0 library for windows with PETSc and MPI support for both Win32 and x64.

It supports:
ZLib, VTK, HDF5, PARMETIS, PETSC, MPI and UMFPACK libraries.

You will need to download these files in downloads section of bitbucket.org:
dolfin_1_4_0_dll_release_1_1_Win32_x64_PETSc.zip
additional_libs_release_1_1.zip

dolfin_1_4_0_dll_release_1_1_Win32_x64_PETSc.zip - contains dolfin 1.4 and other libraries dll files.
additional_libs_release_1_1.zip - contains header, lib and dll files for all other libraries.
_____________________________________________________________________________________


You could download project and then compile a dolfin 1.3 or dolfin 1.4 dll as Debug, Release or both versions. 

For a dolfin dll compilation you also needed a "libxml dll" and a boost libraries. 
You can download the "libxml dll" in the Download section of bitbucket.org.
You can download the boost by using this link:
http://sourceforge.net/projects/boost/files/boost-binaries/1.56.0/
I recommend you to download "boost_1_56_0-bin-msvc-all-32-64.7z" file with all binaries.
Also we only need libraries for msvc-12.0, like "lib32-msvc-12.0" or "lib64-msvc-12.0" (depends on achitecture of computer).
Please extract boost libraries in "C:\boost\" folder (or you will have to change projects settings).
So it will looks like this:
C:\boost\boost_1_56_0

Also you can simple download compiled dolfin dll libraries in the Download section as well.
Currently it is "version_1_0" and the dolfin dll compile with UMFPACK without BLAS or LAPACK.


I have been used this guide to compile the dolfin dll:
http://www.nada.kth.se/~alessio/pmwiki-2.1.27/pmwiki.php?n=Main.Guides

I will compile the dolfin dll with PETSc library in near time.

Also project containts two examples for checking the dolfin dll:
"demo_poisson_dolfin" and "demo_stokes_taylor_hood_dolfin".

For compilation dolfin dll "version_1_0" I use these libraries:
libdolfin_1_3_0, libdolfin_1_4_0 (License: GNU LGPL v3; Link: https://bitbucket.org/fenics-project/dolfin/src);
libamd_2_2_0 (License: GNU LGPL v3; Link: http://faculty.cse.tamu.edu/davis/SuiteSparse/);
libumfpack_5_1_0 (License: GNU LGPL v3, any later version is GPL; Link: http://faculty.cse.tamu.edu/davis/SuiteSparse/);
libxml2_2_9_0 (License: MIT; Link: ftp://xmlsoft.org/libxml2);
boost_1_56_0 (License: Boost Software License; Link: http://sourceforge.net/projects/boost/files/boost-binaries/1.56.0/);

Also I use header files from these libraries:
eigen_3_2 (License: MPL2; Link: http://eigen.tuxfamily.org/);
ffc_1_4_0 (for dolfin 1.4; Link: https://bitbucket.org/fenics-project);
ufc_2_3_0 (for dolfin 1.3; License: public domain; Link: https://bitbucket.org/fenics-project);
UFconfig_3_1_0 (License: GNU LGPL v3; Link: http://faculty.cse.tamu.edu/davis/SuiteSparse/);

To get AMD version 2.2.0, UMFPACK version 5.1.0 and UFconfig version 3.0.0 (version number is the same as SuiteSparse) libraries by the link above you should to download "SuiteSparse-3.0.0.tar.gz" form the site.

You could download all these libraries (all source files) in the Download section as well:)

To compile dolfin dll by yourself you should choose project configuration from the list below:
Debug dynamic library dolfin_1_3_0
Debug dynamic library dolfin_1_4_0
Release dynamic library dolfin_1_3_0
Release dynamic library dolfin_1_4_0





If you have any problem don't hestinate to write me.
If I will have free time, I will fix it as soon as possible.